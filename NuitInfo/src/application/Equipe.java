package application;

import java.util.ArrayList;
import java.util.List;

class Equipe {
    private int id;
    private String nom;
    private List<Equipier> equipiers;

    public Equipe(int id, String nom) {
        this.id = id;
        this.nom = nom;
        this.equipiers = new ArrayList<>();
    }

    // Ajouter un équipier à l'équipe
    public void ajouterEquipier(Equipier equipier) {
        equipiers.add(equipier);
    }

    // Supprimer un équipier de l'équipe
    public void supprimerEquipier(Equipier equipier) {
        equipiers.remove(equipier);
    }

    // Getters et setters
    public int getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public List<Equipier> getEquipiers() {
        return equipiers;
    }
    
    @Override
    public String toString() {
        return nom;
    }
}
