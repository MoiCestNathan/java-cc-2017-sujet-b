package application;

class Defi {
    private int id;
    private String titre;
    private String description;

    public Defi(int id, String titre, String description) {
        this.id = id;
        this.titre = titre;
        this.description = description;
    }

    // Getters et setters
    public int getId() {
        return id;
    }

    public String getTitre() {
        return titre;
    }

    public String getDescription() {
        return description;
    }
    
    @Override
    public String toString() {
        return  titre + ", " + description;
    }
}