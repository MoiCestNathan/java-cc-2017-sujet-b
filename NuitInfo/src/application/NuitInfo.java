package application;

import java.util.ArrayList;
import java.util.List;

class NuitInfo {
    private List<Equipier> participants;
    private List<Equipe> equipes;
    private List<Defi> defis;

    public NuitInfo() {
        participants = new ArrayList<>();
        equipes = new ArrayList<>();
        defis = new ArrayList<>();
    }

    // Méthodes de gestion des participants
    public void ajouterParticipant(Equipier participant) {
        participants.add(participant);
    }

    public void supprimerParticipant(Equipier participant) {
        participants.remove(participant);
    }

    public Equipier chercherParticipant(int id) {
        for (Equipier participant : participants) {
            if (participant.getId() == id) {
                return participant;
            }
        }
        return null;
    }

    // Méthodes de gestion des équipes
    public void ajouterEquipe(Equipe equipe) {
        equipes.add(equipe);
    }

    public void supprimerEquipe(Equipe equipe) {
        equipes.remove(equipe);
    }

    public Equipe chercherEquipe(int id) {
        for (Equipe equipe : equipes) {
            if (equipe.getId() == id) {
                return equipe;
            }
        }
        return null;
    }

    // Méthodes de gestion des défis
    public void ajouterDefi(Defi defi) {
        defis.add(defi);
    }

    public void supprimerDefi(Defi defi) {
        defis.remove(defi);
    }

    public Defi chercherDefi(int id) {
        for (Defi defi : defis) {
            if (defi.getId() == id) {
                return defi;
            }
        }
        return null;
    }
}