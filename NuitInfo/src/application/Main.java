package application;

public class Main {
    public static void main(String[] args) {
        
    	
    	/*---------- Question 3 ----------*/
    	
    	NuitInfo nuitInfo = new NuitInfo();

        Equipier equipier1 = new Equipier(1, "Nathan", null);
        Equipier equipier2 = new Equipier(2, "Alex", null);
        nuitInfo.ajouterParticipant(equipier1);
        nuitInfo.ajouterParticipant(equipier2);

        Equipe equipe1 = new Equipe(1, "Glouzdi");
        Equipe equipe2 = new Equipe(2, "Liste");
        nuitInfo.ajouterEquipe(equipe1);
        nuitInfo.ajouterEquipe(equipe2);

        Defi defi1 = new Defi(1, "Défi1", "Defi boule");
        Defi defi2 = new Defi(2, "Défi2", "Defi tire");
        nuitInfo.ajouterDefi(defi1);
        nuitInfo.ajouterDefi(defi2);

        // Utilisation des méthodes de gestion
        System.out.println("Participant 1 : " + nuitInfo.chercherParticipant(1));
        System.out.println("Equipe 2 : " + nuitInfo.chercherEquipe(2));
        System.out.println("Défi 1 : " + nuitInfo.chercherDefi(1));
    }        	
    	
    	
    	
    	
    	
    	
    	
    	
    	/*---------- Question 1&2 ----------*/

      /* // Création des équipiers
        Equipier equipier1 = new Equipier(1, "Pseudo1", null);
        Equipier equipier2 = new Equipier(2, "Pseudo2", null);

        // Création des équipes
        Equipe equipe1 = new Equipe(1, "Equipe1");
        Equipe equipe2 = new Equipe(2, "Equipe2");

        // Ajout des équipiers aux équipes
        equipe1.ajouterEquipier(equipier1);
        equipe2.ajouterEquipier(equipier2);

        // Attribution des équipes aux équipiers
        equipier1.setEquipe(equipe1);
        equipier2.setEquipe(equipe2);

        // Création des défis
        Defi defi1 = new Defi(1, "Défi1", "Description du défi 1");
        Defi defi2 = new Defi(2, "Défi2", "Description du défi 2");

        // Affichage des informations
        System.out.println("Equipier 1 : " + equipier1.getPseudo() + ", Equipe : " + equipier1.getEquipe().getNom());
        System.out.println("Equipier 2 : " + equipier2.getPseudo() + ", Equipe : " + equipier2.getEquipe().getNom());
        System.out.println("Equipe 1 : " + equipe1.getNom() + ", Equipiers : " + equipe1.getEquipiers());
        System.out.println("Equipe 2 : " + equipe2.getNom() + ", Equipiers : " + equipe2.getEquipiers());
        System.out.println("Défi 1 : " + defi1.getTitre() + ", Description : " + defi1.getDescription());
        System.out.println("Défi 2 : " + defi2.getTitre() + ", Description : " + defi2.getDescription());
    }*/
}