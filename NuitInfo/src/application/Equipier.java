package application;

class Equipier {
    private int id;
    private String pseudo;
    private Equipe equipe;

    public Equipier(int id, String pseudo, Equipe equipe) {
        this.id = id;
        this.pseudo = pseudo;
        this.equipe = equipe;
    }

    // Getters et setters
    public int getId() {
        return id;
    }

    public String getPseudo() {
        return pseudo;
    }

    public Equipe getEquipe() {
        return equipe;
    }

    public void setEquipe(Equipe equipe) {
        this.equipe = equipe;
    }
    
    @Override
    public String toString() {
        return pseudo;
    }
}